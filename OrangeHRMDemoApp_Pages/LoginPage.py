from AutomationFramework.Utilities.Utils import Utils
from AutomationFramework.WebTest import WebTest
from OrangeHRMDemoApp_Pages.BasePage import BasePage


class LoginPage(BasePage):
    def __init__(self,BrowserName):
        #Here we are calling BasePage constructor
        super().__init__()
        Utils.InitialiseEnvVars()
        #self.T=WebTest()
        self.T.StartTest(BrowserName)
        self.T.CreateObjectRepositoy("LoginPage")

    def DoLogin(self,UserName,Passowrd):
        self.T.EnterText(self.T.ObjectRepo["UserNameField"],UserName)
        self.T.EnterText(self.T.ObjectRepo["PasswordField"],Passowrd)
        self.T.ClickElement(self.T.ObjectRepo["SignInBtn"])

    def GetLoginErrorMessage(self):
        ErrorMessage=self.T.FindAndReturnElement("BY_ID","spanMessage")
        return self.T.GetElementText(ErrorMessage)

    def closeBrowser(self):
        self.T.close()