import pytest

from OrangeHRMDemoApp_Pages.LoginPage import LoginPage

#@pytest.mark.usefixtures("OpenApp")
class testLoginFunctionality:
    loginpage=None
    def testLoginFunctionalityWithValidData(self):
        loginpage=testLoginFunctionality.loginpage
        #loginpage=LoginPage("Chrome")
        #loginpage=OpenApp
        loginpage.DoLogin("Admin","admin123")
        DashboardURL=loginpage.GetPageURL()
        assert DashboardURL=="https://opensource-demo.orangehrmlive.com/index.php/dashboard"
